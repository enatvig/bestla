#!/usr/bin/python
#
# Copyright 2015 John H. Dulaney <jdulaney@fedoraproject.org>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import sys
from flask import render_template, flash, redirect, url_for
from bestla import webui, vm_list
from flask.ext.wtf import Form
from wtforms import StringField, BooleanField, TextAreaField
from wtforms.validators import DataRequired
from virt import InstallThread, VirtualMachine, vm_command

ssh_keys = []

@webui.route('/')
@webui.route('/index')
def index():
    """Front page of site"""
    return render_template('index.html', title='Welcome to bestla')

@webui.route('/command/<name>/<command>')
@webui.route('/machines')
def machines(name = None, command = None):
    """Virtual machine management page"""
    vms = []
    for vm in vm_list:
        start_stop_resume = ''
        pause = ''
        delete = ''
        if vm.status != 'installing':
            delete = 'Delete'
        if vm.status == 'running':
            start_stop_resume = 'Stop'
            pause = 'Pause'
        elif vm.status == 'shut off':
            start_stop_resume = 'Start'
        elif vm.status == 'paused':
            start_stop_resume = 'Resume'
        if vm.status != 0:
            vms.append({
                'name': vm.name,
                'status': vm.status,
                'ip': vm.ip,
                'start_stop': start_stop_resume,
                'delete': delete,
                'pause': pause
                })
    if name is not None:
        vm_command(name, command)
        name = None
    return render_template('vms.html', title='Machines', vms=vms)

@webui.route('/new', methods=['GET', 'POST'])
def new():
    virt_form = VirtForm()
    if virt_form.validate_on_submit():
        vm_name = virt_form.name.data
        new_vm = VirtualMachine(vm_name)
        vm_list.append(new_vm)
        ssh_key = ssh_keys.pop()
        ssh_keys.append(ssh_key)
        new_vm.install(ssh_keys)
        return redirect('/index')
    return render_template('virtform.html', title='Create Virtual Machine', 
            form=virt_form)

@webui.route('/ssh', methods=['GET', 'POST'])
def ssh():
    ssh_form = sshForm()
    if ssh_form.validate_on_submit():
        key = ssh_form.key.data
        ssh_keys.append(key)
        return redirect('/index')
    return render_template('ssh.html', title='Paste in SSH Public Key',
            form=ssh_form)


class sshForm(Form):
    key = TextAreaField('key', validators=[DataRequired()])

class VirtForm(Form):
    name = StringField('name', validators=[DataRequired()])


